# Kong-Docker-Grafana

## บทความที่เขียน docker-compose นี้
- [เตรียมพร้อม Deploy API Gateway และ Monitoring Microservices ด้วย Kong และ Grafana](https://www.suseman.biz/deploy-kong-api-gateway-grafana/)

## บทความเพิ่มเติม
- [มาลองเล่น Kong ด้วย Konga กัน แล้วจะรู้ว่า Kong ไม่ได้มีดีแค่เป็น API Gateway ทั่วๆไป (ตอนที่ 1: การตั้งค่า)](https://wisdomgoody.medium.com/%E0%B8%A1%E0%B8%B2%E0%B8%A5%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%99-kong-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-konga-%E0%B8%81%E0%B8%B1%E0%B8%99-%E0%B9%81%E0%B8%A5%E0%B9%89%E0%B8%A7%E0%B8%88%E0%B8%B0%E0%B8%A3%E0%B8%B9%E0%B9%89%E0%B8%A7%E0%B9%88%E0%B8%B2-kong-%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B9%84%E0%B8%94%E0%B9%89%E0%B8%A1%E0%B8%B5%E0%B8%94%E0%B8%B5%E0%B9%81%E0%B8%84%E0%B9%88%E0%B9%80%E0%B8%9B%E0%B9%87%E0%B8%99-api-gateway-%E0%B8%97%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B9%86%E0%B9%84%E0%B8%9B-%E0%B8%95%E0%B8%AD%E0%B8%99%E0%B8%97%E0%B8%B5%E0%B9%88-1-17185899e1d0)
- [มาลองเล่น Kong ด้วย Konga กัน แล้วจะรู้ว่า Kong ไม่ได้มีดีแค่เป็น API Gateway ทั่วๆไป (ตอนที่ 2: ตัวอย่างการใช้ plugin)](https://wisdomgoody.medium.com/%E0%B8%A1%E0%B8%B2%E0%B8%A5%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%A5%E0%B9%88%E0%B8%99-kong-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-konga-%E0%B8%81%E0%B8%B1%E0%B8%99-%E0%B9%81%E0%B8%A5%E0%B9%89%E0%B8%A7%E0%B8%88%E0%B8%B0%E0%B8%A3%E0%B8%B9%E0%B9%89%E0%B8%A7%E0%B9%88%E0%B8%B2-kong-%E0%B9%84%E0%B8%A1%E0%B9%88%E0%B9%84%E0%B8%94%E0%B9%89%E0%B8%A1%E0%B8%B5%E0%B8%94%E0%B8%B5%E0%B9%81%E0%B8%84%E0%B9%88%E0%B9%80%E0%B8%9B%E0%B9%87%E0%B8%99-api-gateway-%E0%B8%97%E0%B8%B1%E0%B9%88%E0%B8%A7%E0%B9%86%E0%B9%84%E0%B8%9B-%E0%B8%95%E0%B8%AD%E0%B8%99%E0%B8%97%E0%B8%B5%E0%B9%88-2-8475f2fe8b40)
- [มาทำระบบ Monitoring ด้วย Prometheus + Grafana กันเถอะ (Part 1)](https://medium.com/@Natthapete/%E0%B8%A1%E0%B8%B2%E0%B8%97%E0%B8%B3%E0%B8%A3%E0%B8%B0%E0%B8%9A%E0%B8%9A-monitoring-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-prometheus-grafana-%E0%B8%81%E0%B8%B1%E0%B8%99%E0%B9%80%E0%B8%96%E0%B8%AD%E0%B8%B0-part-1-41ca6c1f4b9)
- [มาทำระบบ Monitoring ด้วย Prometheus + Grafana กันเถอะ (Part 2)](https://medium.com/@Natthapete/%E0%B8%A1%E0%B8%B2%E0%B8%97%E0%B8%B3%E0%B8%A3%E0%B8%B0%E0%B8%9A%E0%B8%9A-monitoring-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-prometheus-grafana-%E0%B8%81%E0%B8%B1%E0%B8%99%E0%B9%80%E0%B8%96%E0%B8%AD%E0%B8%B0-part-2-3c40d47851c8)


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/penthai06/kong-docker-grafana.git
git branch -M main
git push -uf origin main
```
